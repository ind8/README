---
Title: Welcome to RWX!
Subtitle: Join Our Community
Tags: boost video
Planned: soon
TODO: Need to create a text version of the video.
---

Get to know us, our [values](/values/), [views](/views/),
[occupations](/jobs/), and [how we do things](/logistics/). Join us on
[Discord](https://discord.gg/9wydZXY), IRC,
[Twitch](https://twitch.tv/rwxrob),
[YouTube](https://www.youtube.com/c/rwxrob), and
[GitLab](https://gitlab.com/rwx.gg/README). Understand how our free
knowledge app and site are organized, how to follow along and get the
best from us, then share-alike --- even [contribute](/contrib/) [paying
it forward](https://duck.com/lite?kae=t&q=paying it forward).

[VIDEO](https://youtu.be/CXkms5OUiic)

## See Also {#see}

- [Values](/values/)
- [Views](/views/)
- [Occupations](/jobs/)
- [Logistics](/boosts/oldboost/logistics/)
- [Discord](https://discord.gg/9wydZXY)
- [Twitch](https://twitch.tv/rwxrob)
- [YouTube](https://www.youtube.com/rwxrob)
- [GitLab](https://gitlab.com/rwx.gg/README)
- [Contribute](/contrib/)
