---
Title: Nyan Cat Loop Challenge
Tags: easy
---

Write a command-line program that loops forever printing the word `Nyan`
(or something).

## Requirements

* Must include a safe shebang line if a script.
* When `./nyan` then prints `Nyan` forever.
* Cancel the program after letting run for a while.

## Bonus

* Clear the screen before starting to print.
* Print to a single long line instead of a new line.
* Add color to your output.
* Add *random* color to your output.
* Add a interrupt trap to print a nice exist message.

