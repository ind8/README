---
Title: Views on Life, Learning, and Tech
Query: true
---

The word *view* is often a synonym for *opinion*. Views are inevitably shaped by [values](/values/). Opinions are impossible to escape in life --- particularly in the tech world. Opinions are good. Strong opinions are even better if they have been backed with objective research. But opinions can become toxic when mixed with ego and passion becoming explosive, dogmatic, and personal --- a struggle most every passionate technologist will face at some point. 

Conflict usually comes from not knowing *why* a person has a opinion. In fact, most anger can be linked to misunderstanding. Too often technologists *start with conclusions* without an explanation of how they arrived at them leading to misunderstanding, judgement, anger, and the occasional middle finger.

For example, the following conclusions are very confrontational:

* Microsoft is awesome!
* React sucks!
* Semicolons are useless in modern JavaScript.
* Python is a horrible first language.

A bunch of readers just got angry. Many are wondering immediately which conclusions are "right." More importantly, however, is the question, "Why would someone say these things?"

Even if your conclusion is backed in objective observation and experience starting with it comes off as trolling and personal when given first—especially because so many technologists are afflicted with [like-mentality](/what/likes/) and [intellectual laziness](https://duck.com/lite?kae=t&q=intellectual laziness) when it comes to researching their own conclusions --- or worse --- *sharing* how they arrived at them. Strive to be different. You are better than that.

## Learning

* Creativity, exploration, and controlled failure are key to learning.

* Every human has an innate right to education and learning.

* Every member of society has a responsibility to learn and to teach.

* The most effective members of society are also the strongest [autodidacts](/what/autodidact/).

* Learning material can't keep up without being approached like source code.

* Learning is best one-on-one from someone who has already learned it.

* Learning retention is best when done over time rather than concentrated.

* No one is ever done learning including people who call themselves teachers.

* Age is irrelevant when it comes to learning.

## Systems of Education

* Most schools are deeply flawed because their model fails.

* College Board and the American AP Computer Science program is very broken.

* College provides fundamental skills that can be obtained elsewhere.

* Colleges should drop all assessment entry requirements.

* College-level education should be free for everyone.

## Coding

* Every technologist must learn to code.

* Knowledge must be treated as code.

* Pandoc Markdown is the best syntax for knowledge source.

* Using the Linux command linen *is* coding.

* The most [important coding languages](/lang/) include structured data and knowledge.

* The best first language is Bash since 

* [Python](/lang/python/) is a *horrible* first language, but an useful secondary language.

* The [cognitive overhead](/cognitive-overhead/) of JavaScript [semicolons](/semicolon/) justifies omitting them.

## Knowledge



### Web Libraries and Frameworks

* React.js is bad for the Web because it throws out established standards.

* Vue.js is better than React.js because it is simpler and follows standards.

* [JAMstack](https://jamstack.com) hosted on [Netlify](https://netlify.com) is the best approach to web development for most.

### Tools

* Every [full-stack engineer](/fse/) should master both the graphical editor [VSCode](/vscode/) and the command line editor [Vim](/vim/) but *should use* Vim as much as possible.

* Every offensive security professional should also master the `ed` editor.

### Careers in Technology

* Unfortunately some sort of college degree is still required by most.

* Many technical careers do *not* require college but certifications help.

* The school one attends should have nothing to do with being hired.

* White-board interviews are inaccurate assessments of employability.

* White-board interviews are ethically wrong.

* Employability and performance should be measured by merit and results.
