---
Title: What is a Terminal User Interface / TUI?
Subtitle: A Way to Use a Program That Usess All of the Terminal Screen
Query: true
---

A *terminal user interface* or *TUI* is an [interface](/what/interface/) that allows you to control something, usually a computer or device through a full screen terminal, not just a single [command line](../command). TUIs are standard for [servers](/what/server/) that don't waste resources on a full [graphic interface](../graphic/)
