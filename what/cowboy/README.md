---
Title: What is a Tech Cowboys / Maverick / Rogue?
Subtitle: Doing Stuff Your Way, Until Your Fired or Promoted
Query: true
---

Practicing *over-zealous* pro-activity --- the opposite of [not doing anything until you are told](/advice/engage/) --- doing whatever you think is right and *cutting the decision maker entirely out of the loop* --- can be dangerous to your career depending on your situation. Techs who do this are often labelled *cowboys*, a more pejorative term compared to another similar one we saw a lot of some years back when [political *mavericks* McCain and Palin ran for office](https://duck.com/lite?kae=t&q=political *mavericks* McCain and Palin ran for office). 

Usually such people are branded as "non-team players" and eventually removed unless somehow they are actually better than everyone else *and can safely convince the higher-ups that their ideas are superior* in which case they often get promoted making a bunch of new enemies in the process. Maybe you don't care. But it's a risky gamble that's not for everyone. Usually making yourself *and your team* look good is the safer strategy --- especially if you are just starting out. People who get stepped on tend to have long memories.
