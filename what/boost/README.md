---
Title: What is a boost?
Subtitle: A Push to Get You Going
---

A *boost* is the thing parents give to their kids when teaching them how to ride a bicycle, a bit of initial momentum to help them get their balance and direction. One you give a boost you *get out of the way* and let the person moving take over.

:::co-pwz
The concept of a boost must be fully understood and used when the focus is on autodidactic learning --- which stuffy people might prefer to call *an alternative pedagogical approach.* The priority and responsibility are *always* with the person doing the learning --- not the ["teacher"](/what/teacher/) or mentor.
:::
