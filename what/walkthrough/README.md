---
Title: Walkthrough
Subtitle: Going Through the Steps
Query: true
---

A *walkthrough* is a step-by-step description of the steps required to complete a process with full explanations along the way. In many ways a walkthrough is a form of programming humans. In fact, a more formal term for the same thing is a *procedure*, a term popular in the military and other places where a strict way of doing things is required. It is no surprise that these same terms are used in computer science since education is the science and process of programming human brains.
