---
Title: Visual Studio Code / VSCode
Query: true
---

[*Visual Studio Code*](https://code.visualstudio.com), as Microsoft calls it, or *VSCode* to the rest of us, is currently the leading [GUI](/what/hci/ui/graphic/) [editor](/tools/editors/). VSCode runs on top of the [Electron](/what/electron/) platform created by [GitHub](/services/github/) to power their [Atom](/tools/editors/atom/) editor. Like the rest of the minimal GUI editors to appear VSCode was heavily inspired from Atom and originally [Sublime](/tools/editors/sublime/).

:::co-warning
Do not confuse *Visual Studio Code* with *Visual Studio*.
:::
