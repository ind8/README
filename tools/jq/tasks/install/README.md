---
Title: Install `jq`, The JSON Command Line Processing Tool
Query: now
---

The `jq` command is a part of the standard software package repositories so it requires very little effort to install.

```sh
sudo apt install jq
```
